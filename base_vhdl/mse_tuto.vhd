-------------------------------------------------------------------------------
-- Title       : MSE Tuto
-- Project     : MSE Tuto
-------------------------------------------------------------------------------
-- File        : mse_tuto
-- Authors     : Joachim Schmidt
-- Company     : Hepia
-- Created     : --
-- Last update : --
-- Platform    : Vivado (synthesis)
-- Standard    : VHDL'08
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyright (c) 2018 Hepia, Geneve
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- --           0.0      SCJ      Created
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--library work;

entity mse_tuto is

    port (
        ClkSys100MhzxC : in  std_logic;
        RstxR          : in  std_logic;
        LedxD          : out std_logic_vector(7 downto 0));

end entity mse_tuto;

architecture behavioural of mse_tuto is

    -- Generic memory
    constant C_WORD_SIZE    : integer := 8;
    -- constant C_WORD_SIZE    : integer := 32;
    constant C_MEMORY_DEPTH : integer := 8;
    -- constant C_MEMORY_DEPTH : integer := 1048576;

    subtype WordNBitsxD is std_logic_vector((C_WORD_SIZE - 1) downto 0);
    type MemoryNxA is array ((C_MEMORY_DEPTH - 1) downto 0) of WordNBitsxD;

    signal MemxD : MemoryNxA := (others => (others => '0'));

    -- Math. Op.
    constant C_MULT_SIZE : integer                                      := C_WORD_SIZE * 2;
    signal VectAxD       : std_logic_vector((C_WORD_SIZE - 1) downto 0) := (others => '0');
    signal VectBxD       : std_logic_vector((C_WORD_SIZE - 1) downto 0) := (others => '1');
    signal VectResAddxD  : std_logic_vector((C_WORD_SIZE - 1) downto 0) := (others => '0');
    signal VectResMultxD : std_logic_vector((C_MULT_SIZE - 1) downto 0) := (others => '0');

    -- Others signal
    constant C_COUNTER_SIZE    : integer                                         := 8;
    constant C_COUNTER_MAX_VAL : integer                                         := 2**C_COUNTER_SIZE;
    signal CounterxD           : std_logic_vector((C_COUNTER_SIZE - 1) downto 0) := (others => '0');
    signal DataCounterxD       : std_logic_vector((C_WORD_SIZE - 1) downto 0)    := (others => '0');

    -- Debugs signal
    -- attribute mark_debug                  : string;
    -- attribute mark_debug of VectAxD       : signal is "true";
    -- attribute mark_debug of VectBxD       : signal is "true";
    -- attribute mark_debug of VectResAddxD  : signal is "true";
    -- attribute mark_debug of VectResMultxD : signal is "true";
    -- attribute mark_debug of CounterxD     : signal is "true";
    -- attribute mark_debug of DataCounterxD : signal is "true";

    -- attribute keep                  : string;
    -- attribute keep of VectAxD       : signal is "true";
    -- attribute keep of VectBxD       : signal is "true";
    -- attribute keep of VectResAddxD  : signal is "true";
    -- attribute keep of VectResMultxD : signal is "true";
    -- attribute keep of CounterxD     : signal is "true";
    -- attribute keep of DataCounterxD : signal is "true";

begin  -- architecture behavioural

    -- Asynchronous statements

    -- Synchronous statements

    MemCountxP : process (all) is
    begin  -- process MemCountxP

        if RstxR = '1' then
            CounterxD     <= (others => '0');
            DataCounterxD <= (others => '0');
            VectAxD       <= (others => '0');
            VectBxD       <= (others => '1');
        elsif rising_edge(ClkSys100MhzxC) then
            CounterxD     <= CounterxD;
            DataCounterxD <= DataCounterxD;
            VectAxD       <= std_logic_vector(unsigned(VectAxD) + 1);
            VectBxD       <= std_logic_vector(unsigned(VectBxD) - 1);

            if to_integer(unsigned(CounterxD)) < C_MEMORY_DEPTH then
                CounterxD <= std_logic_vector(unsigned(CounterxD) + 1);
            else
                CounterxD <= (others => '0');
            end if;

            if to_integer(unsigned(DataCounterxD)) < C_COUNTER_MAX_VAL then
                DataCounterxD <= std_logic_vector(unsigned(DataCounterxD) + 1);
            else
                DataCounterxD <= (others => '0');
            end if;
        end if;

    end process MemCountxP;

    WriteMemxP : process (all) is
    begin  -- process WriteMemxP

        if rising_edge(ClkSys100MhzxC) then
            if (to_integer(unsigned(CounterxD)) mod 2) = 0 then
                MemxD(to_integer(unsigned(CounterxD))) <= VectResAddxD;
            else
                MemxD(to_integer(unsigned(CounterxD))) <= VectResMultxD((C_WORD_SIZE - 1) downto 0);
            end if;
        end if;

    end process WriteMemxP;

    ReadMemxP : process (all) is
    begin  -- process ReadMemxP

        if RstxR = '1' then
            LedxD <= (others => '0');
        elsif rising_edge(ClkSys100MhzxC) then
            LedxD <= MemxD(to_integer(unsigned(CounterxD)))(7 downto 0);
        end if;

    end process ReadMemxP;

    AddxP : process (all) is
    begin  -- process AddxP

        if RstxR = '1' then
            VectResAddxD <= (others => '0');
        elsif rising_edge(ClkSys100MhzxC) then
            VectResAddxD <= std_logic_vector(unsigned(VectAxD) + unsigned(VectBxD));
        end if;

    end process AddxP;

    MultxP : process (all) is
    begin  -- process MultxP

        if RstxR = '1' then
            VectResMultxD <= (others => '0');
        elsif rising_edge(ClkSys100MhzxC) then
            VectResMultxD <= std_logic_vector(unsigned(VectAxD) * unsigned(VectBxD));
        end if;

    end process MultxP;

end architecture behavioural;
