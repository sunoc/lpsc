LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY ci_grayscale IS
PORT (
    clk: IN std_logic;
    clk_en: IN std_logic;
    start: IN std_logic;
    reset: IN std_logic;
    data_a: IN std_logic_vector(31 downto 0);
    data_b: IN std_logic_vector(31 downto 0);
    result: OUT std_logic_vector(31 downto 0);
    done: OUT std_logic);
END ci_grayscale;