LIBRARY IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;

ARCHITECTURE behave OF ci_grayscale is
   type GrayscaleStates_t is (st_idle, st_1);
   SIGNAL state, nxt_state : GrayscaleStates_t;

BEGIN
   gray_clk_proc : process(clk, clk_en, nxt_state, reset)
   begin
      if(rising_edge(clk)) then
         if(reset = '1') then
            state <= st_idle;
         elsif((reset = '0') AND (clk_en = '1')) then
            state <= nxt_state;
         else
            state <= st_idle;
         end if;
      end if;
   end process;

   fsm_impl : process(state, data_a, data_b, start)

      variable s_red1: std_logic_vector(7 downto 0) := (others=>'0');
      variable s_red2: std_logic_vector(7 downto 0) := (others=>'0');
      variable s_red3: std_logic_vector(7 downto 0) := (others=>'0');
      variable s_red4: std_logic_vector(7 downto 0) := (others=>'0');
      variable s_green1: std_logic_vector(7 downto 0) := (others=>'0');
      variable s_green2: std_logic_vector(7 downto 0) := (others=>'0');
      variable s_green3: std_logic_vector(7 downto 0) := (others=>'0');
      variable s_green4: std_logic_vector(7 downto 0) := (others=>'0');
      variable s_blue1: std_logic_vector(7 downto 0) := (others=>'0');
      variable s_blue2: std_logic_vector(7 downto 0) := (others=>'0');
      variable s_blue3: std_logic_vector(7 downto 0) := (others=>'0');
      variable s_blue4: std_logic_vector(7 downto 0) := (others=>'0');

      variable s_tmp_red1: unsigned(15 downto 0) := (others => '0');
      variable s_tmp_red2: unsigned(15 downto 0) := (others => '0');
      variable s_tmp_red3: unsigned(15 downto 0) := (others => '0');
      variable s_tmp_red4: unsigned(15 downto 0) := (others => '0');

      variable s_tmp_green1: unsigned(15 downto 0) := (others => '0');
      variable s_tmp_green2: unsigned(15 downto 0) := (others => '0');
      variable s_tmp_green3: unsigned(15 downto 0) := (others => '0');
      variable s_tmp_green4: unsigned(15 downto 0) := (others => '0');

      variable s_tmp_blue1: unsigned(15 downto 0) := (others => '0');
      variable s_tmp_blue2: unsigned(15 downto 0) := (others => '0');
      variable s_tmp_blue3: unsigned(15 downto 0) := (others => '0');
      variable s_tmp_blue4: unsigned(15 downto 0) := (others => '0');

      variable s_tmp_gray1: std_logic_vector(15 downto 0) := (others => '0');
      variable s_tmp_gray2: std_logic_vector(15 downto 0) := (others => '0');
      variable s_tmp_gray3: std_logic_vector(15 downto 0) := (others => '0');
      variable s_tmp_gray4: std_logic_vector(15 downto 0) := (others => '0');
   begin
      -- Default action
      nxt_state <= state;

      case state is 
         when st_idle =>
            -- RGB565: Put those values in correctly sized elements
            s_red1(7 downto 3) := data_a(15 downto 11);
            s_red2(7 downto 3) := data_a(31 downto 27);
            s_red3(7 downto 3) := data_b(15 downto 11);
            s_red4(7 downto 3) := data_b(31 downto 27);

            -- Green
            s_green1(7 downto 2) := data_a(10 downto 5);
            s_green2(7 downto 2) := data_a(26 downto 21);
            s_green3(7 downto 2) := data_b(10 downto 5);
            s_green4(7 downto 2) := data_b(26 downto 21);

            -- Blue
            s_blue1(7 downto 3) := data_a(4 downto 0);
            s_blue2(7 downto 3) := data_a(20 downto 16);
            s_blue3(7 downto 3) := data_b(4 downto 0);
            s_blue4(7 downto 3) := data_b(20 downto 16);

            -- red *27
            s_tmp_red1 := unsigned(s_red1) * 27;
            s_tmp_red2 := unsigned(s_red2) * 27;
            s_tmp_red3 := unsigned(s_red3) * 27;
            s_tmp_red4 := unsigned(s_red4) * 27;
            -- green *92
            s_tmp_green1 := unsigned(s_green1) * 92;
            s_tmp_green2 := unsigned(s_green2) * 92;
            s_tmp_green3 := unsigned(s_green3) * 92;
            s_tmp_green4 := unsigned(s_green4) * 92;
            -- blue *9
            s_tmp_blue1 := unsigned(s_blue1) * 9;
            s_tmp_blue2 := unsigned(s_blue2) * 9;
            s_tmp_blue3 := unsigned(s_blue3) * 9;
            s_tmp_blue4 := unsigned(s_blue4) * 9;

            -- Outputs
            done <= '0';
            result(31 downto 0) <= (others => '0');
            -- Next step
            if(start = '1') then
               nxt_state <= st_1;
            else
               nxt_state <= st_idle;
            end if;

         when st_1 =>
            s_tmp_gray1 := std_logic_vector((s_tmp_red1 + s_tmp_green1 + s_tmp_blue1) srl 7);
            s_tmp_gray2 := std_logic_vector((s_tmp_red2 + s_tmp_green2 + s_tmp_blue2) srl 7);
            s_tmp_gray3 := std_logic_vector((s_tmp_red3 + s_tmp_green3 + s_tmp_blue3) srl 7);
            s_tmp_gray4 := std_logic_vector((s_tmp_red4 + s_tmp_green4 + s_tmp_blue4) srl 7);

            -- Outputs
            done <= '1';
            result(7 downto 0) <= s_tmp_gray1(7 downto 0);
            result(15 downto 8) <= s_tmp_gray2(7 downto 0);
            result(23 downto 16) <= s_tmp_gray3(7 downto 0);
            result(31 downto 24) <= s_tmp_gray4(7 downto 0);

            -- Next step
            nxt_state <= st_idle;

      end case;
   end process;
END behave;