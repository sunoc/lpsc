library vunit_lib;
context vunit_lib.vunit_context;

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use STD.TEXTIO.ALL;

entity c_grayscale_tb is
  generic (runner_cfg : string);
end entity;

architecture tb of c_grayscale_tb is

  signal s_clk, s_clk_en, s_start, s_reset, s_done : std_logic := '0';
  signal s_data_a, s_data_b : std_logic_vector(31 downto 0) := (others => '0');
  signal s_result : std_logic_vector(31 downto 0) := (others => '0');
  constant clk_period : time := 10 ns;
begin
	-- Instantiate the Unit Under Test (UUT)
    uut : entity work.ci_grayscale port map(
      clk => s_clk,
      clk_en => s_clk_en,
      start => s_start,
      reset => s_reset,
      data_a => s_data_a, 
      data_b => s_data_b, 
      result => s_result,
      done => s_done
    );

  clk_proc: process
  begin
    s_clk <= '0';
    wait for clk_period/2;
    s_clk <= '1';
    wait for clk_period/2;
  end process;

  main : process
  begin
    set_stop_count(error, 2);
    test_runner_setup(runner, runner_cfg);

    while test_suite loop
      -- Default values for each test
      s_data_a <= x"00000000";
      s_data_b <= x"00000000";
      s_clk_en <= '0';
      s_reset <= '1';
      s_start <= '0';

      if run("Reset and clocking") then
        -- Initial conditions
        s_clk_en <= '0';
        s_reset <= '1';
        -- Test 1
        wait until rising_edge(s_clk);
        check_equal(unsigned(s_result) , 0, "Reset should yield 0.");
        check_equal(s_done , '0', "Expected done to be 0.");

        -- After reset
        wait for clk_period/2;
        s_reset <= '0';
        s_clk_en <= '0';

        wait until rising_edge(s_clk);
        check_equal(unsigned(s_result) , 0, "After reset + with clock disabled should yield 0.");
        check_equal(s_done , '0', "Expected done to be 0.");

        -- Check startup
        wait for clk_period/2;
        s_reset <= '0';
        s_clk_en <= '1';

        wait until rising_edge(s_clk);
        check_equal(unsigned(s_result) , 0, "After reset + with clock enabled should yield 0.");
        check_equal(s_done , '0', "Expected done to be 0.");
        s_reset <= '0';
        s_clk_en <= '1';

        -- Load data
        wait for clk_period/2;
        s_data_a <= x"0000CAFE";
        s_data_b <= x"00000000";

        wait until rising_edge(s_clk);
        check_equal(unsigned(s_result) , 0, "Even if an input is given, it should not return any value.");
        check_equal(s_done , '0', "Expected done to be 0 in st_idle.");

      elsif run("Pixel 1 calculation") then
        -- Initial conditions
        s_clk_en <= '1';
        s_reset <= '0';

        -- Check if a single 16 bit RGB is correctly converted.
        wait until rising_edge(s_clk);
        s_data_a <= x"0000CAFE";
        s_data_b <= x"00000000";
        s_start <= '1';

        wait until rising_edge(s_clk);
        check_equal(s_done , '0', "Expected done to be 0 in idle.");
        check_equal(unsigned(s_result) , 0, "Grayscale should be 0.");
        s_start <= '0';

        -- Check results
        wait until rising_edge(s_done);
        check_equal(unsigned(s_result) , 125, "Grayscale value is wrong.");

      elsif run("Pixel 2 calculation") then
        -- Initial conditions
        s_clk_en <= '1';
        s_reset <= '0';

        -- Test the second parameter of the SIMD
        wait until rising_edge(s_clk);
        s_start <= '1';
        s_data_a <= x"00000000";
        s_data_b <= x"0000CAFE";
        check_equal(unsigned(s_result) , 0, "Grayscale should be 0 right after loading data.");

        wait until rising_edge(s_done);
        check_equal(unsigned(s_result), 8192000, "Grayscale value is wrong.");

      elsif run("Pixel SIMD validity check") then
        -- Initial conditions
        s_clk_en <= '1';
        s_reset <= '0';

        -- Feed 4 16bit RGB pixel (SIMD) in the instruction
        wait until rising_edge(s_clk);
        s_start <= '1';
        s_data_a <= x"CAFECAFE";
        s_data_b <= x"CAFECAFE";
        check_equal(s_done , '0', "Expected done to be 0 in idle.");
        check_equal(unsigned(s_result) , 0, "Grayscale should be 0.");

        -- Check results
        wait until rising_edge(s_done);
        check_equal(unsigned(s_result), 2105376125, "Grayscale value is wrong.");

      -- elsif run("Timing requirements") then
      --   -- Initial conditions
      --   s_clk_en <= '1';
      --   s_reset <= '0';

      --   wait until rising_edge(s_clk);
      --   wait for clk_period/2;
      --   s_data_a <= x"CAFECAFE";
      --   s_data_b <= x"CAFECAFE";
      --   s_start <= '1';

      --   wait until rising_edge(s_clk);
      --   wait until rising_edge(s_clk);
      --   check_equal(unsigned(s_result), 2105376125, "Grayscale value is wrong.");

      end if;
      
    end loop;
  
    test_runner_cleanup(runner); -- Simulation ends here
  end process;
end architecture;