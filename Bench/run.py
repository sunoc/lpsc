from vunit import VUnit

# Create VUNit instance from custom arguments
vu = VUnit.from_argv()

# Create VUnit instance by parsing command line arguments
# vu = VUnit.from_argv()
# vu.add_osvvm()
# vu.add_verification_components()

# Create library 'lib'
lib = vu.add_library("mywork")

# Add all files ending in .vhd in current working directory to library
lib.add_source_files("./rtl/*.vhdl")
lib.add_source_files("./bench/*.vhdl")

# Run vunit function
vu.main()
