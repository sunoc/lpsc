README
------

## Requiremens
- Python 3
- PIP
- Vunit
- GHDL or modelsim (Available in the PATH env. variable)

## Installing 

> pip install vunit_hdl

## Running

To run tests:
> py ./run.py

Simulate with GUI (Modelsim/GHDL):
>py ./run.py -g
