(TeX-add-style-hook
 "CONUS_LPSC_Rapport"
 (lambda ()
   (setq TeX-command-extra-options
         "-shell-escape")
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("report" "a4paper" "titlepage")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("fontenc" "T1") ("inputenc" "utf8x") ("easylist" "ampersand") ("footmisc" "bottom")))
   (TeX-run-style-hooks
    "latex2e"
    "report"
    "rep10"
    "fontenc"
    "graphicx"
    "float"
    "mathtools"
    "listings"
    "inputenc"
    "verbatim"
    "hyperref"
    "enumitem"
    "easylist"
    "minted"
    "caption"
    "footmisc")
   (LaTeX-add-labels
    "sec:mandelbrot"
    "fig:mandel"
    "sec:xilinx-artix-7"
    "fig:artix"
    "sec:mach-detat-mand"
    "sec:partie-calcul"
    "sec:implementation-de-la"
    "sec:banc-de-test"
    "sec:generation-dimage"
    "sec:integration-de-la"
    "sec:generateur-de-nombre"
    "sec:struct-gener-du"
    "sec:mappage-avec-la"
    "sec:gestion-des-couleurs"
    "sec:conclusion"
    "sec:diff-renc"
    "sec:benefices--concepts"))
 :latex)

