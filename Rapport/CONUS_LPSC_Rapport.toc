\contentsline {chapter}{\numberline {1}Travail pratique}{2}{chapter.1}
\contentsline {section}{\numberline {1.1}Introduction}{2}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}Mandelbrot}{2}{subsection.1.1.1}
\contentsline {subsection}{\numberline {1.1.2}Xilinx Artix-7}{3}{subsection.1.1.2}
\contentsline {section}{\numberline {1.2}Machine d'\IeC {\'e}tat Mandelbrot}{4}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}Partie calcul}{4}{subsection.1.2.1}
\contentsline {subsection}{\numberline {1.2.2}Impl\IeC {\'e}mentation de la machine d'\IeC {\'e}tat}{5}{subsection.1.2.2}
\contentsline {subsection}{\numberline {1.2.3}Banc de test pour la machine d'\IeC {\'e}tat}{6}{subsection.1.2.3}
\contentsline {section}{\numberline {1.3}G\IeC {\'e}n\IeC {\'e}ration d'image}{6}{section.1.3}
\contentsline {subsection}{\numberline {1.3.1}Int\IeC {\'e}gration de la machine d'\IeC {\'e}tat}{7}{subsection.1.3.1}
\contentsline {subsection}{\numberline {1.3.2}G\IeC {\'e}n\IeC {\'e}rateur de nombre complexe}{7}{subsection.1.3.2}
\contentsline {section}{\numberline {1.4}Structure g\IeC {\'e}n\IeC {\'e}rale du projet \& domaines d'horloge}{8}{section.1.4}
\contentsline {subsection}{\numberline {1.4.1}Mappage avec la BRAM}{8}{subsection.1.4.1}
\contentsline {subsection}{\numberline {1.4.2}Gestion des couleurs de la fractale}{8}{subsection.1.4.2}
\contentsline {subsection}{\numberline {1.4.3}Informations g\IeC {\'e}n\IeC {\'e}rale sur le syst\IeC {\`e}me \& l'impl\IeC {\'e}mentation}{9}{subsection.1.4.3}
\contentsline {section}{\numberline {1.5}Conclusion}{10}{section.1.5}
\contentsline {subsection}{\numberline {1.5.1}Parties perfectibles}{10}{subsection.1.5.1}
\contentsline {paragraph}{Bug d'affichage}{10}{section*.12}
\contentsline {paragraph}{Pr\IeC {\'e}cision de la machine d'\IeC {\'e}tat}{10}{section*.13}
\contentsline {subsection}{\numberline {1.5.2}Difficult\IeC {\'e}s rencontr\IeC {\'e}es}{10}{subsection.1.5.2}
\contentsline {paragraph}{Vivado}{10}{section*.14}
\contentsline {paragraph}{Clef bootable et git}{10}{section*.15}
\contentsline {paragraph}{Structure g\IeC {\'e}n\IeC {\'e}rale du projet}{11}{section*.16}
\contentsline {subsection}{\numberline {1.5.3}B\IeC {\'e}n\IeC {\'e}fices \& concepts appris}{11}{subsection.1.5.3}
