library vunit_lib;
context vunit_lib.vunit_context;

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use STD.TEXTIO.ALL;

entity mandelbrotcalc_tb is
  generic (runner_cfg : string);
end entity;

architecture tb of mandelbrotcalc_tb is

  signal s_clk, s_reset, s_ready, s_start, s_finished : std_logic := '0';
  signal s_creal, s_cimag, s_zreal, s_zimag, s_iter : std_logic_vector(15 downto 0) := (others => '0');
  signal s_result : std_logic_vector(31 downto 0) := (others => '0');
  constant clk_period : time := 10 ns;
begin
	-- Instantiate the Unit Under Test (UUT)
    uut : entity work.mandelbrot_calculator port map(
      -- logic
      clk => s_clk,
      rst => s_reset,
      ready => s_ready,
      start => s_start,
      finished => s_finished,
      -- vector
      c_real => s_creal,
      c_imaginary => s_cimag,
      z_real => s_zreal,
      z_imaginary => s_zimag,
      iterations => s_iter
    );

  clk_proc: process
  begin
    s_clk <= '0';
    wait for clk_period/2;
    s_clk <= '1';
    wait for clk_period/2;
  end process;

  main : process
  begin
    set_stop_count(error, 2);
    test_runner_setup(runner, runner_cfg);

    while test_suite loop
      -- Default values for each test
      s_creal <= x"0000";
      s_cimag <= x"0000";

      s_reset <= '0';
      s_start <= '0';

      if run("Reset") then
        s_reset <= '1';

        wait until rising_edge(s_clk);
        wait until rising_edge(s_clk);
        wait until rising_edge(s_clk);
        --wait until rising_edge(s_clk);      
        check_equal(signed(s_zreal), 0, "Real == 0");
        check_equal(signed(s_zimag), 0, "Imaginary == 0");
        check_equal(s_ready, '1', "Ready == 1");
        check_equal(s_finished, '0', "Finished == 0");

        -- After reset
        wait for clk_period/2;
        s_reset <= '0';

        wait until rising_edge(s_clk);
        check_equal(signed(s_zreal), 0, "Real == 0");
        check_equal(signed(s_zimag), 0, "Imaginary == 0");
        check_equal(s_ready, '1', "Ready == 1");
        check_equal(s_finished, '0', "Finished == 0");

        -- Load data
        wait for clk_period/2;
        s_creal <= x"0100";
        s_cimag <= x"0010";

        wait until rising_edge(s_clk);
        check_equal(s_ready, '1', "Ready == 1");
        check_equal(s_finished, '0', "Finished == 0");

        wait until rising_edge(s_clk);
        check_equal(s_ready, '1', "Ready == 1");
        check_equal(s_finished, '0', "Finished == 0");

      elsif run("Maximum boucle") then
        s_reset <= '1';
        wait until rising_edge(s_clk);
        s_reset <= '0';

        wait until rising_edge(s_clk);
        s_start <= '1';

        wait until rising_edge(s_clk);
        s_start <= '0';

        wait until rising_edge(s_finished);

        check_equal(s_iter, 100, "Iteration max == 100");
        check_equal(s_zreal, 0, "Z real == 0");
        check_equal(s_zimag, 0, "Z imag == 0");

        wait until rising_edge(s_clk);
        wait until rising_edge(s_clk);
        wait until rising_edge(s_clk);
        wait until rising_edge(s_clk);

      elsif run("Position (1,0)") then
        s_reset <= '1';
        wait until rising_edge(s_clk);
        s_reset <= '0';
        wait until rising_edge(s_clk);

        wait for clk_period/2;
        s_creal <= x"1000";
        s_cimag <= x"0000";

        wait until rising_edge(s_clk);
        s_start <= '1';

        wait until rising_edge(s_clk);
        s_start <= '0';

        wait until rising_edge(s_finished);

        check_equal(s_iter, 3, "Iteration == 3");
        check_equal(s_zreal, 20480, "Z real == 5");
        check_equal(s_zimag, 0, "Z imag == 0");
        
        wait until rising_edge(s_clk);
        wait until rising_edge(s_clk);
        wait until rising_edge(s_clk);
        wait until rising_edge(s_clk);

      elsif run("Position (0,-2)") then
        s_reset <= '1';
        wait until rising_edge(s_clk);
        s_reset <= '0';
        wait until rising_edge(s_clk);

        wait for clk_period/2;
        s_creal <= x"0000";
        s_cimag <= x"E000";

        wait until rising_edge(s_clk);
        s_start <= '1';

        wait until rising_edge(s_clk);
        s_start <= '0';

        wait until rising_edge(s_finished);

        check_equal(s_iter, 2, "Iteration == 2");
        check_equal(s_zreal, 49152, "Z real == -4");
        check_equal(s_zimag, 57344, "Z imag == -2");
        
        wait until rising_edge(s_clk);
        wait until rising_edge(s_clk);
        wait until rising_edge(s_clk);
        wait until rising_edge(s_clk);

      elsif run("Position (0.3125,0.040039062)") then
        s_reset <= '1';
        wait until rising_edge(s_clk);
        s_reset <= '0';
        wait until rising_edge(s_clk);

        wait for clk_period/2;
        s_creal <= x"0500";
        s_cimag <= x"00A4";

        wait until rising_edge(s_clk);
        s_start <= '1';

        wait until rising_edge(s_clk);
        s_start <= '0';

        wait until rising_edge(s_finished);

        check_equal(s_iter, 100, "Iteration == 100");
        check_equal(s_zreal, 2064, "Z real == 0.504642");--0000.0110 0100 0000
        check_equal(s_zimag, 1720, "Z imag == 0.419798");--0000.0111 0110 1000

        wait until rising_edge(s_clk);
        wait until rising_edge(s_clk);
        wait until rising_edge(s_clk);
        wait until rising_edge(s_clk);
        
      elsif run("Position (0.3594,0.5)") then
        s_reset <= '1';
        wait until rising_edge(s_clk);
        s_reset <= '0';
        wait until rising_edge(s_clk);

        wait for clk_period/2;
        s_creal <= x"0640";
        s_cimag <= x"0800";

        wait until rising_edge(s_clk);
        s_start <= '1';

        wait until rising_edge(s_clk);
        s_start <= '0';

        wait until rising_edge(s_finished);

        check_equal(s_iter, 7, "Iteration == 7");
        check_equal(s_zreal, 2064, "Z real == -1.39439592727");
        check_equal(s_zimag, 1720, "Z imag == 1.63897284912");

        wait until rising_edge(s_clk);
        wait until rising_edge(s_clk);
        wait until rising_edge(s_clk);
        wait until rising_edge(s_clk);

      end if;

    end loop;

    test_runner_cleanup(runner); -- Simulation ends here
  end process;
end architecture;
