library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity mandelbrot_calculator is
  generic ( comma    : integer := 12;  -- nombre de bits après la virgule
            max_iter : integer := 100; -- nombre max d'itération 
            SIZE     : integer := 16); -- taille des logic vectors 
  port(
    clk         : in    std_logic;
    rst         : in    std_logic;
    ready       : out   std_logic;
    start       : in    std_logic;
    finished    : out   std_logic;
    c_real      : in    std_logic_vector(SIZE-1 downto 0);
    c_imaginary : in    std_logic_vector(SIZE-1 downto 0);
    z_real      : out   std_logic_vector(SIZE-1 downto 0);
    z_imaginary : out   std_logic_vector(SIZE-1 downto 0);
    iterations  : out   std_logic_vector(SIZE-1 downto 0)
    );
end mandelbrot_calculator;

architecture behave of mandelbrot_calculator is
  --- pour la machine d'état
  type state_t is (s_ready, s_compute, s_done);
  signal cur_state	: state_t := s_ready;
  signal next_state	: state_t := s_ready;

  -- compteur pour le nombre d'itérations
  signal n     : integer := 0; 

  --- signaux de sortie
  signal iterations_s : std_logic_vector(SIZE-1 downto 0) := (others => '0');
  signal finished_s   : std_logic := '0';
  signal ready_s      : std_logic := '0';
  signal znr   : std_logic_vector(SIZE-1 downto 0) := (others => '0'); 
  signal zni   : std_logic_vector(SIZE-1 downto 0) := (others => '0'); 


begin

  --- process to change the state of the FSM
  --- It also manages the reset
  p_state: process (clk, rst)
  begin   
    if (rising_edge(clk)) then
      if (rst = '1')then
        cur_state <= s_ready;
      else
        cur_state <= next_state;
      end if;      
    end if;

    --- updates the outputs
    iterations       <= iterations_s;
    finished         <= finished_s;
    ready            <= ready_s;
    z_real           <= znr;
    z_imaginary      <= zni;
  end process p_state;
  

  --- process than manages the FSM (states actions and next states)
--  p_fsm: process (clk, n, cur_state, start, znr, zni)
  p_fsm: process (clk)
    --- buffer de 32 bits pour les multiplications
    variable multBuf: std_logic_vector((2*SIZE)-1 downto 0) := (others => '0');

    --- variables intéermédiaires de calcul
    --variable znr2  : std_logic_vector(SIZE-1 downto 0) := (others => '0'); -- ZnR au carré
    --variable zni2  : std_logic_vector(SIZE-1 downto 0) := (others => '0'); -- ZnI au carré
    variable zn2   : std_logic_vector(SIZE-1 downto 0) := (others => '0'); -- ZnR * ZnI
    --variable znsub : std_logic_vector(SIZE-1 downto 0) := (others => '0'); -- ZnR^2 - ZnI^2
  
    variable znrp1 : std_logic_vector(SIZE-1 downto 0) := (others => '0'); -- Z(n+1)R
    variable znip1 : std_logic_vector(SIZE-1 downto 0) := (others => '0'); -- Z(n+1)I

    variable norm  : std_logic_vector(SIZE-1 downto 0) := (others => '0'); -- Z(n+1)I
    
  begin

    if (rising_edge(clk)) then

      case cur_state is
        --- FSM ready to compute again (no real tasks?)
        --- tourne sur lui même until start
        --- then go to compute
        when s_ready   =>
          if (start = '1') then
            next_state <= s_compute;
            
            --- reset all the values
            n <= 0;
            znr <= (others => '0');
            zni <= (others => '0'); 
          else
            next_state <= s_ready;
          end if;

          --- apply the output to the signals
          finished_s    <= '0';
          iterations_s  <= (others => '0');
          ready_s       <= '1';       
          
          
        --- calculates the Z(n+1)R et Z(n+1)I
        --- fait le calcul et vérifie les conditions de fin
        --- reste dans compute si conditions pas ok
        --- passe dans done qudn ok
        when s_compute =>
          
          --- calculate the norme of the vecto
          --- for the limit verification
          multBuf             := std_logic_vector((signed(znrp1) *  signed(znrp1)) +
                                                  (signed(znip1) *  signed(znip1)));
          --norm(15)            := multBuf(31) or multBuf(30);
          norm(15)            := '0'; -- the result can only be positive
          norm(14 downto 0)   := multBuf(26 downto 12);

          -- the comma incliudes the sign bit!
          norm := std_logic_vector(shift_right(signed(norm), comma));
          
          --- condition de sortie
          --- On gère la virgule en passant par des non-signé qui sont shiftés
          --- de 12 bits (comma), puis comparé à la valeur cible: 2^2 = 4
          if (to_integer(signed(norm)) >= 4) or  (n >= max_iter-1) then
          --if (to_integer(signed(norm)) >= 16384) then
            next_state <= s_done;
          --- condition de sortie pour le timeout
          --elsif  then --- pour un max de 100 itérations
          --  next_state <= s_done;
          else
            next_state <= s_compute; --- cycle suivant
          end if;


          
          --- calcul pour l'étape suivant    
          -- Pour les multiplications, il faut passer par une variable 32 bit
          -- (multBuf) afin de gérer les dépassement de taille. 
          multBuf      := std_logic_vector((signed(znr) *  signed(znr)) -
                                           (signed(zni) *  signed(zni)));
          --- bit(s) de signe à gérer
          znrp1(15)            := multBuf(31) or multBuf(30);
          --- /!\ Partie un peu tricky
          --- Reste de la valeure calculée
          -- Sur le vectueur de 32 bits créé par la multiplication,
          -- on a 2 bit de signes6 bits avant la virgule
          -- et 24 bits après.
          -- On veut garder seulement 3 bits après la virgule
          -- et ce qu'on garde après sert à compléter les 16 bits,
          -- d'où les bits 26 à 12 qui sont gardés.
          znrp1(14 downto 0)   := multBuf(26 downto 12);
          --- partie réelle
          znrp1                := std_logic_vector(signed(znrp1) + signed(c_real));


          
          -- Une autre multiplication qui passe par la variable
          -- 32bit multBuf
          multBuf     := std_logic_vector(shift_left(signed(znr) * signed(zni), 1));
          --- bit(s) de signe
          --zn2(15)             := multBuf(31) or multBuf(30);
          --- Même chose que ci-dessus
          --zn2(14 downto 0)    := multBuf(26 downto 12);

          
          
          
          --- Même chose que ci-dessus pour la partie imaginaire
          --multBuf              := std_logic_vector(shift_left(signed(zn2), 1));          
          znip1(15)            := multBuf(31) or multBuf(30);
          znip1(14 downto 0)   := multBuf(26 downto 12);
          --- partie imaginaire
          znip1                := std_logic_vector(signed(znip1) + signed(c_imaginary));

          
          --- the flip-flop part
          znr     <= znrp1;
          zni     <= znip1;

          
          --- apply the output to the signals
          finished_s    <= '0';
          iterations_s  <= (others => '0');
          ready_s       <= '0';  

          --- increments the iteration counter
          n <= n + 1; 

        when s_done    =>
          
          next_state <= s_ready; --- on retourne à l'état "ready" après ça

          --- apply the output to the signals
          finished_s    <= '1';
          iterations_s <= std_logic_vector(to_unsigned(n, SIZE));
          n <= 0; --- resets the iteration counter

          ready_s       <= '0';
          

        --- in case of trouble, go to ready (default)
        when others    =>
          next_state <= s_ready;
      end case;

    end if;
    
  end process p_fsm;

end behave;    
