# 
# Synthesis run script generated by Vivado
# 

set TIME_start [clock seconds] 
proc create_report { reportName command } {
  set status "."
  append status $reportName ".fail"
  if { [file exists $status] } {
    eval file delete [glob $status]
  }
  send_msg_id runtcl-4 info "Executing : $command"
  set retval [eval catch { $command } msg]
  if { $retval != 0 } {
    set fp [open $status w]
    close $fp
    send_msg_id runtcl-5 warning "$msg"
  }
}
set_param xicom.use_bs_reader 1
create_project -in_memory -part xc7a200tsbg484-1

set_param project.singleFileAddWarning.threshold 0
set_param project.compositeFile.enableAutoGeneration 0
set_param synth.vivado.isSynthRun true
set_msg_config -source 4 -id {IP_Flow 19-2162} -severity warning -new_severity info
set_property webtalk.parent_dir /home/quartus/git/lpsc/Mandelbrot/designs/vivado/mandelbrot_pinout/2018.2/mandelbrot_pinout.cache/wt [current_project]
set_property parent.project_path /home/quartus/git/lpsc/Mandelbrot/designs/vivado/mandelbrot_pinout/2018.2/mandelbrot_pinout.xpr [current_project]
set_property XPM_LIBRARIES {XPM_CDC XPM_MEMORY} [current_project]
set_property default_lib xil_defaultlib [current_project]
set_property target_language VHDL [current_project]
set_property ip_repo_paths /home/quartus/git/lpsc/Mandelbrot [current_project]
set_property ip_output_repo /home/quartus/git/lpsc/Mandelbrot/designs/vivado/mandelbrot_pinout/2018.2/mandelbrot_pinout.cache/ip [current_project]
set_property ip_cache_permissions {read write} [current_project]
add_files /home/quartus/git/lpsc/useful_vhdl_files/color_distributed_mem.coe
read_vhdl -library xil_defaultlib {
  /home/quartus/git/lpsc/useful_vhdl_files/ComplexValueGenerator.vhd
  /home/quartus/git/lpsc/Mandelbrot/ips/hw/hdmi/src/hdl_pkg/hdmi_interface_pkg.vhd
  /home/quartus/git/lpsc/useful_vhdl_files/mandelbrot_calc.vhd
}
read_vhdl -vhdl2008 -library xil_defaultlib {
  /home/quartus/git/lpsc/Mandelbrot/ips/hw/cdc_sync/src/hdl/cdc_sync.vhd
  /home/quartus/git/lpsc/Mandelbrot/ips/hw/hdmi/src/hdl/vga_stripes.vhd
  /home/quartus/git/lpsc/Mandelbrot/ips/hw/hdmi/src/hdl/vga_controler.vhd
  /home/quartus/git/lpsc/Mandelbrot/ips/hw/hdmi/src/hdl/vga.vhd
  /home/quartus/git/lpsc/Mandelbrot/ips/hw/hdmi/src/hdl/tmds_encoder.vhd
  /home/quartus/git/lpsc/Mandelbrot/ips/hw/hdmi/src/hdl/serializer_10_to_1.vhd
  /home/quartus/git/lpsc/Mandelbrot/ips/hw/hdmi/src/hdl/vga_to_hdmi.vhd
  /home/quartus/git/lpsc/Mandelbrot/ips/hw/hdmi/src/hdl/hdmi.vhd
  /home/quartus/git/lpsc/Mandelbrot/ips/hw/image_generator/src/hdl/image_generator.vhd
  /home/quartus/git/lpsc/Mandelbrot/ips/hw/ublaze/src/hdl/ublaze_core.vhd
  /home/quartus/git/lpsc/Mandelbrot/designs/hw/mandelbrot_pinout/src/hdl/mandelbrot_pinout.vhd
}
add_files /home/quartus/git/lpsc/Mandelbrot/ips/vivado/ublaze/2018.2/ublaze.srcs/sources_1/bd/ublaze_sopc/ublaze_sopc.bd
set_property used_in_implementation false [get_files -all /home/quartus/git/lpsc/Mandelbrot/ips/vivado/ublaze/2018.2/ublaze.srcs/sources_1/bd/ublaze_sopc/ip/ublaze_sopc_microblaze_0_0/ublaze_sopc_microblaze_0_0.xdc]
set_property used_in_implementation false [get_files -all /home/quartus/git/lpsc/Mandelbrot/ips/vivado/ublaze/2018.2/ublaze.srcs/sources_1/bd/ublaze_sopc/ip/ublaze_sopc_microblaze_0_0/ublaze_sopc_microblaze_0_0_ooc_debug.xdc]
set_property used_in_implementation false [get_files -all /home/quartus/git/lpsc/Mandelbrot/ips/vivado/ublaze/2018.2/ublaze.srcs/sources_1/bd/ublaze_sopc/ip/ublaze_sopc_microblaze_0_0/ublaze_sopc_microblaze_0_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/quartus/git/lpsc/Mandelbrot/ips/vivado/ublaze/2018.2/ublaze.srcs/sources_1/bd/ublaze_sopc/ip/ublaze_sopc_clk_wiz_0_0/ublaze_sopc_clk_wiz_0_0_board.xdc]
set_property used_in_implementation false [get_files -all /home/quartus/git/lpsc/Mandelbrot/ips/vivado/ublaze/2018.2/ublaze.srcs/sources_1/bd/ublaze_sopc/ip/ublaze_sopc_clk_wiz_0_0/ublaze_sopc_clk_wiz_0_0.xdc]
set_property used_in_implementation false [get_files -all /home/quartus/git/lpsc/Mandelbrot/ips/vivado/ublaze/2018.2/ublaze.srcs/sources_1/bd/ublaze_sopc/ip/ublaze_sopc_clk_wiz_0_0/ublaze_sopc_clk_wiz_0_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/quartus/git/lpsc/Mandelbrot/ips/vivado/ublaze/2018.2/ublaze.srcs/sources_1/bd/ublaze_sopc/ip/ublaze_sopc_proc_sys_reset_0_0/ublaze_sopc_proc_sys_reset_0_0_board.xdc]
set_property used_in_implementation false [get_files -all /home/quartus/git/lpsc/Mandelbrot/ips/vivado/ublaze/2018.2/ublaze.srcs/sources_1/bd/ublaze_sopc/ip/ublaze_sopc_proc_sys_reset_0_0/ublaze_sopc_proc_sys_reset_0_0.xdc]
set_property used_in_implementation false [get_files -all /home/quartus/git/lpsc/Mandelbrot/ips/vivado/ublaze/2018.2/ublaze.srcs/sources_1/bd/ublaze_sopc/ip/ublaze_sopc_proc_sys_reset_0_0/ublaze_sopc_proc_sys_reset_0_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/quartus/git/lpsc/Mandelbrot/ips/vivado/ublaze/2018.2/ublaze.srcs/sources_1/bd/ublaze_sopc/ip/ublaze_sopc_lmb_v10_0_0/ublaze_sopc_lmb_v10_0_0.xdc]
set_property used_in_implementation false [get_files -all /home/quartus/git/lpsc/Mandelbrot/ips/vivado/ublaze/2018.2/ublaze.srcs/sources_1/bd/ublaze_sopc/ip/ublaze_sopc_lmb_v10_0_0/ublaze_sopc_lmb_v10_0_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/quartus/git/lpsc/Mandelbrot/ips/vivado/ublaze/2018.2/ublaze.srcs/sources_1/bd/ublaze_sopc/ip/ublaze_sopc_lmb_v10_0_1/ublaze_sopc_lmb_v10_0_1.xdc]
set_property used_in_implementation false [get_files -all /home/quartus/git/lpsc/Mandelbrot/ips/vivado/ublaze/2018.2/ublaze.srcs/sources_1/bd/ublaze_sopc/ip/ublaze_sopc_lmb_v10_0_1/ublaze_sopc_lmb_v10_0_1_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/quartus/git/lpsc/Mandelbrot/ips/vivado/ublaze/2018.2/ublaze.srcs/sources_1/bd/ublaze_sopc/ip/ublaze_sopc_lmb_bram_if_cntlr_0_0/ublaze_sopc_lmb_bram_if_cntlr_0_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/quartus/git/lpsc/Mandelbrot/ips/vivado/ublaze/2018.2/ublaze.srcs/sources_1/bd/ublaze_sopc/ip/ublaze_sopc_blk_mem_gen_0_0/ublaze_sopc_blk_mem_gen_0_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/quartus/git/lpsc/Mandelbrot/ips/vivado/ublaze/2018.2/ublaze.srcs/sources_1/bd/ublaze_sopc/ip/ublaze_sopc_lmb_bram_if_cntlr_0_1/ublaze_sopc_lmb_bram_if_cntlr_0_1_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/quartus/git/lpsc/Mandelbrot/ips/vivado/ublaze/2018.2/ublaze.srcs/sources_1/bd/ublaze_sopc/ip/ublaze_sopc_mdm_0_0/ublaze_sopc_mdm_0_0.xdc]
set_property used_in_implementation false [get_files -all /home/quartus/git/lpsc/Mandelbrot/ips/vivado/ublaze/2018.2/ublaze.srcs/sources_1/bd/ublaze_sopc/ip/ublaze_sopc_mdm_0_0/ublaze_sopc_mdm_0_0_ooc_trace.xdc]
set_property used_in_implementation false [get_files -all /home/quartus/git/lpsc/Mandelbrot/ips/vivado/ublaze/2018.2/ublaze.srcs/sources_1/bd/ublaze_sopc/ip/ublaze_sopc_mdm_0_0/ublaze_sopc_mdm_0_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/quartus/git/lpsc/Mandelbrot/ips/vivado/ublaze/2018.2/ublaze.srcs/sources_1/bd/ublaze_sopc/ip/ublaze_sopc_axi_gpio_0_0/ublaze_sopc_axi_gpio_0_0_board.xdc]
set_property used_in_implementation false [get_files -all /home/quartus/git/lpsc/Mandelbrot/ips/vivado/ublaze/2018.2/ublaze.srcs/sources_1/bd/ublaze_sopc/ip/ublaze_sopc_axi_gpio_0_0/ublaze_sopc_axi_gpio_0_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/quartus/git/lpsc/Mandelbrot/ips/vivado/ublaze/2018.2/ublaze.srcs/sources_1/bd/ublaze_sopc/ip/ublaze_sopc_axi_gpio_0_0/ublaze_sopc_axi_gpio_0_0.xdc]
set_property used_in_implementation false [get_files -all /home/quartus/git/lpsc/Mandelbrot/ips/vivado/ublaze/2018.2/ublaze.srcs/sources_1/bd/ublaze_sopc/ip/ublaze_sopc_axi_timer_0_0/ublaze_sopc_axi_timer_0_0.xdc]
set_property used_in_implementation false [get_files -all /home/quartus/git/lpsc/Mandelbrot/ips/vivado/ublaze/2018.2/ublaze.srcs/sources_1/bd/ublaze_sopc/ip/ublaze_sopc_axi_timer_0_0/ublaze_sopc_axi_timer_0_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/quartus/git/lpsc/Mandelbrot/ips/vivado/ublaze/2018.2/ublaze.srcs/sources_1/bd/ublaze_sopc/ip/ublaze_sopc_axi_intc_0_0/ublaze_sopc_axi_intc_0_0.xdc]
set_property used_in_implementation false [get_files -all /home/quartus/git/lpsc/Mandelbrot/ips/vivado/ublaze/2018.2/ublaze.srcs/sources_1/bd/ublaze_sopc/ip/ublaze_sopc_axi_intc_0_0/ublaze_sopc_axi_intc_0_0_clocks.xdc]
set_property used_in_implementation false [get_files -all /home/quartus/git/lpsc/Mandelbrot/ips/vivado/ublaze/2018.2/ublaze.srcs/sources_1/bd/ublaze_sopc/ip/ublaze_sopc_axi_intc_0_0/ublaze_sopc_axi_intc_0_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/quartus/git/lpsc/Mandelbrot/ips/vivado/ublaze/2018.2/ublaze.srcs/sources_1/bd/ublaze_sopc/ip/ublaze_sopc_xbar_0/ublaze_sopc_xbar_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/quartus/git/lpsc/Mandelbrot/ips/vivado/ublaze/2018.2/ublaze.srcs/sources_1/bd/ublaze_sopc/ublaze_sopc_ooc.xdc]

read_ip -quiet /home/quartus/git/lpsc/Mandelbrot/designs/vivado/mandelbrot_pinout/2018.2/mandelbrot_pinout.srcs/sources_1/ip/clk_vga_hdmi_1024x600/clk_vga_hdmi_1024x600.xci
set_property used_in_implementation false [get_files -all /home/quartus/git/lpsc/Mandelbrot/designs/vivado/mandelbrot_pinout/2018.2/mandelbrot_pinout.srcs/sources_1/ip/clk_vga_hdmi_1024x600/clk_vga_hdmi_1024x600_board.xdc]
set_property used_in_implementation false [get_files -all /home/quartus/git/lpsc/Mandelbrot/designs/vivado/mandelbrot_pinout/2018.2/mandelbrot_pinout.srcs/sources_1/ip/clk_vga_hdmi_1024x600/clk_vga_hdmi_1024x600.xdc]
set_property used_in_implementation false [get_files -all /home/quartus/git/lpsc/Mandelbrot/designs/vivado/mandelbrot_pinout/2018.2/mandelbrot_pinout.srcs/sources_1/ip/clk_vga_hdmi_1024x600/clk_vga_hdmi_1024x600_ooc.xdc]

read_ip -quiet /home/quartus/git/lpsc/Mandelbrot/designs/vivado/mandelbrot_pinout/2018.2/mandelbrot_pinout.srcs/sources_1/ip/fifo_regport/fifo_regport.xci
set_property used_in_implementation false [get_files -all /home/quartus/git/lpsc/Mandelbrot/designs/vivado/mandelbrot_pinout/2018.2/mandelbrot_pinout.srcs/sources_1/ip/fifo_regport/fifo_regport.xdc]
set_property used_in_implementation false [get_files -all /home/quartus/git/lpsc/Mandelbrot/designs/vivado/mandelbrot_pinout/2018.2/mandelbrot_pinout.srcs/sources_1/ip/fifo_regport/fifo_regport_clocks.xdc]
set_property used_in_implementation false [get_files -all /home/quartus/git/lpsc/Mandelbrot/designs/vivado/mandelbrot_pinout/2018.2/mandelbrot_pinout.srcs/sources_1/ip/fifo_regport/fifo_regport_ooc.xdc]

read_ip -quiet /home/quartus/git/lpsc/Mandelbrot/designs/vivado/mandelbrot_pinout/2018.2/mandelbrot_pinout.srcs/sources_1/ip/bram_video_memory_wauto_dauto_rdclk1_wrclk1/bram_video_memory_wauto_dauto_rdclk1_wrclk1.xci
set_property used_in_implementation false [get_files -all /home/quartus/git/lpsc/Mandelbrot/designs/vivado/mandelbrot_pinout/2018.2/mandelbrot_pinout.srcs/sources_1/ip/bram_video_memory_wauto_dauto_rdclk1_wrclk1/bram_video_memory_wauto_dauto_rdclk1_wrclk1_ooc.xdc]

read_ip -quiet /home/quartus/git/lpsc/Mandelbrot/designs/vivado/mandelbrot_pinout/2018.2/mandelbrot_pinout.srcs/sources_1/ip/clk_mandelbrot/clk_mandelbrot.xci
set_property used_in_implementation false [get_files -all /home/quartus/git/lpsc/Mandelbrot/designs/vivado/mandelbrot_pinout/2018.2/mandelbrot_pinout.srcs/sources_1/ip/clk_mandelbrot/clk_mandelbrot_board.xdc]
set_property used_in_implementation false [get_files -all /home/quartus/git/lpsc/Mandelbrot/designs/vivado/mandelbrot_pinout/2018.2/mandelbrot_pinout.srcs/sources_1/ip/clk_mandelbrot/clk_mandelbrot.xdc]
set_property used_in_implementation false [get_files -all /home/quartus/git/lpsc/Mandelbrot/designs/vivado/mandelbrot_pinout/2018.2/mandelbrot_pinout.srcs/sources_1/ip/clk_mandelbrot/clk_mandelbrot_ooc.xdc]

read_ip -quiet /home/quartus/git/lpsc/Mandelbrot/designs/vivado/mandelbrot_pinout/2018.2/mandelbrot_pinout.srcs/sources_1/ip/dist_mem_gen_0/dist_mem_gen_0.xci
set_property used_in_implementation false [get_files -all /home/quartus/git/lpsc/Mandelbrot/designs/vivado/mandelbrot_pinout/2018.2/mandelbrot_pinout.srcs/sources_1/ip/dist_mem_gen_0/dist_mem_gen_0_ooc.xdc]

# Mark all dcp files as not used in implementation to prevent them from being
# stitched into the results of this synthesis run. Any black boxes in the
# design are intentionally left as such for best results. Dcp files will be
# stitched into the design at a later time, either when this synthesis run is
# opened, or when it is stitched into a dependent implementation run.
foreach dcp [get_files -quiet -all -filter file_type=="Design\ Checkpoint"] {
  set_property used_in_implementation false $dcp
}
read_xdc /home/quartus/git/lpsc/Mandelbrot/designs/hw/mandelbrot_pinout/src/constrs/mandelbrot_pinout.xdc
set_property used_in_implementation false [get_files /home/quartus/git/lpsc/Mandelbrot/designs/hw/mandelbrot_pinout/src/constrs/mandelbrot_pinout.xdc]

read_xdc dont_touch.xdc
set_property used_in_implementation false [get_files dont_touch.xdc]
set_param ips.enableIPCacheLiteLoad 0
close [open __synthesis_is_running__ w]

synth_design -top mandelbrot_pinout -part xc7a200tsbg484-1


# disable binary constraint mode for synth run checkpoints
set_param constraints.enableBinaryConstraints false
write_checkpoint -force -noxdef mandelbrot_pinout.dcp
create_report "synth_1_synth_report_utilization_0" "report_utilization -file mandelbrot_pinout_utilization_synth.rpt -pb mandelbrot_pinout_utilization_synth.pb"
file delete __synthesis_is_running__
close [open __synthesis_is_complete__ w]
